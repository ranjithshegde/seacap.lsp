local c = {}

c.config = {}

c.init = false

function c.setup(conf)
    if conf then
        assert(type(conf) == "table")
        c.config = conf
        c.init = true
    end
end

function c.display()
    if c.init then
        require "SeaCap.capabilities"(c.config)
    else
        require "SeaCap.capabilities"()
    end
end

return c
