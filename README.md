# SeaCap.lsp

A small plugin to parse the available `server_capabilities` for all attached Language Servers for a buffer and display them in a floating window. It also formats the capabilities into JSON

## Installation

Install it using your favourite plugin manager.

Here is an example for `packer.nvim`

```lua
use {"https://gitlab.com/ranjithshegde/SeaCap.lsp"}
```

## Configuration

The plugin is setup using `require("SeaCap).setup()` function which takes a table.
The table should contain the same key-values as `nvim_open_win`'s config option

If you wish to use default values, simply do not call the setup function.

## Usage

The plugin is launched either with the lua method `require("SeaCap).display()` or using the `ex-command` `LspCapabilities`

It comes with default bindings to quit with `q`
